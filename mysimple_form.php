<!DOCTYPE html>
<html>
	<head>
		<title>FORM</title>
			<style type="text/css">
				.form_container {
			width: 960px;
			min-height: 500px;
			background: lightpink;
			margin: 0 auto;
 
		}
 
		.f_label {
			width: 400px;
			float: left;
			text-align: right;
			margin-right: 10px;
			margin-bottom: 8px
			}
 
		.f_element {
			width: 540px;
			float: right;
			margin-left: 10px;
			margin-bottom: 5px;
			}

			</style>
	</head>

	<body>

		<div class="form_container" id="">
			<form method="POST" action="add_form_function.php">
				<h1 align="center">Profile</h1>
				<div>
					<div class="f_label">
						<label for="firstname"> First Name :</label>
                	</div>
                	<div class="f_element">
                            <input type="text" placeholder="Insert your first name"  name="first_name">
                	</div>
                </div>

                <div>
                	<div class="f_label">
                		<label for="lastname">Last Name :</label>
                	</div>
                	<div class="f_element">
                            <input type="text" placeholder="Insert your last name"  name="last_name">
                	</div>
                </div>

                <div>
                	<div class="f_label">
                		<label for="Sex">Sex:</label>
                	</div>
                	<div class="f_element">
                		<input type="radio" value="male" name="sex">Male
                		<input type="radio" value="female" name="sex">Female
                	</div>
                </div>

                <div>
                	<div class="f_label">
                		<label for="Age">Age:</label>
                	</div>
                	<div class="f_element">
                		<select name="age" id="age">
                		<option value="">18</option>
                		<option value="">19</option>
                		<option value="">20</option>
                		<option value="">21</option>
                		<option value="">22</option>
                		<option value="">23</option>
                	</select>
                	</div>
                </div>

                <div>
                	<div class="f_label">
                		<label for="interested">Interested:</label>
                	</div>
                	<div class="f_element">
                		<input type="checkbox" name="int">Web Design
                		<input type="checkbox" name="int">Web Development
                		<input type="Checkbox" name="int">Wordpress Theme Development
                		<input type="checkbox" name="int">SEO
                		
                	</div>
                </div>

                <div>
                	<div class="f_label">
                		<label for="comment">Comment</label>
                	</div>
                	<div class="f_element">
                		<textarea name="comment" id="" cols="30" rows="2">
                			Write further comments.
                		</textarea>
                	</div>
                </div>

                <div>
                	<div class="f_element">
                	<input type="submit" value="Submit">
                	<input type="reset" value="Reset">
                	</div>
                </div>
                </form>
		</div>
	</body>
</html>

