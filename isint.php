<?php

$values = array(44,3.4,"33","23.5",NULL,true,FALSE);

foreach ($values as $value) {
    echo "is_int(";
    var_export($value);
    echo ") = ";
    var_dump(is_int($value));
}
echo '<br/>';

$a = 10;
$v = 2.3;
$c = "";

if(is_int($a)){
    echo 'This is a integer';
}  else {
    echo 'This is not a integer';
}
echo '<br/>';

if(is_int($v)){
    echo 'This is a integer';
}  else {
    echo 'This is not a integer';
}
echo '<br/>';

if(is_int($c)){
    echo 'This is a integer';
}  else {
    echo 'This is not a integer';
}
echo '<br/>';