<?php

$file = "my.txt";
$my_imported_data = file_get_contents($file);
echo gettype($my_imported_data);// by gettype function we can find which type of data stored
echo "$my_imported_data";
echo '<br>';
$file1 = "new.txt";
if(file_exists($file1)){//here we are using file_exists function to ensure whether the file exists
    $my_get_data = file_get_contents($file1);
    echo "$my_get_data";// here we will see what is written in that file1
}

