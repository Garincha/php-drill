<?php

$month = "january";
if($month == "january"){
    echo 'Yes, it does.';
}  else {
    echo 'No, it does not.';
}
echo '<br>';
$a = "hello";
if($a == true){// here true works as if this string true the value.Its true,cause $a is not empty,it has a value(string).if $a were empty, it would be false.
    echo 'Yes, it does.';
}  else {
    echo 'NO, it does not.';
}
echo '<br>';
$a = "";
if($a == true){//here $a is empty. thats why it is not true(its false).
    echo 'Yes, it does.';
}  else {
    echo 'NO, it does not.';
}
echo '<br>';
$b = "march";
if($b === true){//here the data type of the variable is not matching.
    echo 'Yes, it does.';
}  else {
    echo 'NO, it does not.';
}
echo '<br>';
$c = "march";
if($c === "march"){//here the data type of the variabie is matching.
    echo 'Yes, it does.';
}  else {
    echo 'NO, it does not.';
}