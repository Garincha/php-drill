<?php

$values = array(NULL, FALSE, true,'abc',23,'23',23.5,'23.5',0,'0','',' ');
foreach ($values as $value) {
    echo "is_string(";
    var_export($value);
    echo ")=";
    echo var_dump(is_string($value));
}
echo '<br/>';
$a = 12;
$b = 'sdf';
$c = "0";
$d = "123.87";
$e = "djf";

if (is_string($a)) {
    echo 'This is a string';
}  else {
    echo 'This is not a string';
}
echo '<br/>';
if (is_string($b)) {
    echo 'This is a string';
}  else {
    echo 'This is not a string';
}
echo '<br/>';
if (is_string($c)) {
    echo 'This is a string';
}  else {
    echo 'This is not a string';
}
echo '<br/>';
if (is_string($d)) {
    echo 'This is a string';
}  else {
    echo 'This is not a string';
}
echo '<br/>';
if (is_string($e)) {
    echo 'This is a string';
}  else {
    echo 'This is not a string';
}
echo '<br/>';