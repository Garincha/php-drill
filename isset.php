<?php


$var = "";
//this will evaluate to true so the next will be printed 
if(isset($var)){
    echo 'This is variable is set';
}
//In the next example we'll use var_dump to output the return value of iset()

$a = "test";
$b = "anothertest";
var_dump(isset($a));
var_dump(isset($a,$b));
unset($a);
var_dump(isset($a));
var_dump(isset($a,$b));
$foo = NULL;
var_dump(isset($foo));
