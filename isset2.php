<?php

$data = 234;

echo isset($data);/*here isset functions ensures that whether $data is set or not and it returns 1.cause when
 a functions condition becomes true it returns 1.
 */
echo '<br/>';
$a = 222;
if (isset($a)) {
    echo 'It is set';
}  else {
    echo 'It is not';
}
echo '<br/>';
$b = null;
if (isset($b)) {
    echo 'it is set';
}
 else {
    echo 'it is not';
}
echo '<br/>';

$v = '';
if (isset($v) && !empty($v)) {// checking 2 condition. 1.is it set and 2.it has to be not empty
    echo 'yes this is set value';
}  else {
    echo 'No data provided.';
}
