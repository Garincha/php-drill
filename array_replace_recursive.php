<?php

$a = array("a" => "red","b" => "blue","c" => "green");
$b = array("a" => "yellow","b" => "black");

print_r(array_replace_recursive($b, $a));
echo '</br>';
print_r(array_replace_recursive($a, $b));
echo '</br>';

$c = array("a" => array("pink"), "g" => array("ash","black"));
$d = array("r" => array("blue"),"t" => array("red"));

print_r(array_replace_recursive($d, $c));
echo '</br>';
print_r(array_replace_recursive($c, $d));
