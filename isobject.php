<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//declare a simple function to return an array from our object
function get_students($obj)
{
    if(!is_object($obj)){
        return FALSE;
    }
    return $obj -> students;
}
// declare a new class instance and fill up some values
$obj = new stdClass();
$obj -> students = array('kalle','ross','felipe');
var_dump(get_students(null));
var_dump(get_students($obj));
